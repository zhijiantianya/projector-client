/*
 * MIT License
 *
 * Copyright (c) 2019-2021 JetBrains s.r.o.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.jetbrains.projector.client.web.state

import org.jetbrains.projector.client.web.WindowSizeController
import org.jetbrains.projector.common.protocol.toServer.ClientEvent

sealed class ClientAction {

  /**
   * 启动 Action
   * 在 Application 的 start 发起
   * 被 ClientState 的 UninitializedPage 消费
   */
  class Start(
    val stateMachine: ClientStateMachine,
    val url: String,
    val windowSizeController: WindowSizeController,
  ) : ClientAction()

  /**
   * WebSocket Action
   * 在 ClientState 的 createWebSocketConnection 发起
   */
  sealed class WebSocket : ClientAction() {

    class Open(val openingTimeStamp: Int) : WebSocket() // 被 ClientState 的 WaitingOpening 消费
    class Message(val message: ByteArray) : WebSocket() // 被 ClientState 的 WaitingHandshakeReply、ReadyToDraw 消费
    class Close(val wasClean: Boolean, val code: Short, val reason: String) : WebSocket()
    class NoReplies(val elapsedTimeMs: Int) : WebSocket()

  }

  /**
   * TODO 芋艿：AddEvent
   */
  class AddEvent(val event: ClientEvent) : ClientAction()

  /**
   * TODO 芋艿：Flush
   */
  object Flush : ClientAction()

  /**
   * 加载字体 Action
   * 在 ClientState 的 WaitingHandshakeReply 发起
   * 被 ClientState 的 LoadingFonts 消费
   */
  object LoadAllFonts : ClientAction()

  /**
   * TODO 芋艿：WindowResize
   */
  object WindowResize : ClientAction()

}
