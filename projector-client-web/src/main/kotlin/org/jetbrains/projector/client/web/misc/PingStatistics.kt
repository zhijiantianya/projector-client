/*
 * MIT License
 *
 * Copyright (c) 2019-2021 JetBrains s.r.o.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.jetbrains.projector.client.web.misc

import kotlinx.browser.window
import org.jetbrains.projector.client.common.misc.ParamsProvider
import org.jetbrains.projector.client.common.misc.TimeStamp
import org.jetbrains.projector.client.web.debug.DivPingShower
import org.jetbrains.projector.client.web.debug.NoPingShower
import org.jetbrains.projector.common.protocol.toClient.ServerPingReplyEvent

/**
 * Ping 统计，最终被 PingShower 暂时
 *
 * @param openingTimeStamp websocket 打开的时间，由客户端生成。可在 ClientState 的 createWebSocketConnection 方法看到。
 */
class PingStatistics(
  private val openingTimeStamp: Int,
  private val requestPing: () -> Unit,
) {

  private val pingShower = when (ParamsProvider.SHOW_PING) {
    true -> DivPingShower()
    false -> NoPingShower
  }

  private var interval: Int? = null

  fun onHandshakeFinished() {
    interval = window.setInterval(
      handler = requestPing,
      timeout = ParamsProvider.PING_INTERVAL
    )

    pingShower.onHandshakeFinished()
  }

  // 注意，因为 Server 是通过 UpdateTime 每 10 毫秒，返回一次结果，所以至少会有 10 毫秒的额外延迟
  fun onPingReply(pingReply: ServerPingReplyEvent) {
    // 当前的相对时间
    val currentTimeStamp = TimeStamp.current.toInt() - openingTimeStamp
    // 从前端发起请求，到请求接收到请求，总耗时（存在 10 毫秒的延迟)
    val ping = currentTimeStamp - pingReply.clientTimeStamp
    // 添加到统计【重要】
    ClientStats.simplePingAverage.add(ping.toDouble())

    // 前端到服务端的消耗时间（不存在 10 毫秒的延迟）（前端和后端，会存在时间的偏差）
    val clientToServer = pingReply.serverReadEventTimeStamp - pingReply.clientTimeStamp
    // 服务端到前端的消耗时间（存在 10 毫秒的延迟)（前端和后端，会存在时间的偏差）
    val serverToClient = currentTimeStamp - pingReply.serverReadEventTimeStamp

    // 展示效果
    pingShower.onPingReply(clientToServer, serverToClient, ping)
  }

  fun onClose() {
    interval?.let { window.clearInterval(handle = it) }
    interval = null

    pingShower.onClose()
  }
}
